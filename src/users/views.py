from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegistrationForm

def home_view(request):
    return render(request, 'home.html')

def register_view(request):
    # check if POST request
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)
        
        # check if data passed validation then display username
        if form.is_valid():
            form.save()
            messages.success(request, f'Your account has been created! Please login!')
            return redirect('login')

    else: # just render the form
        form = UserRegistrationForm()
    
    context = {
        'form': form        
    }

    return render(request, 'users/register.html', context)